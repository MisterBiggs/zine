# Anson's Blogroll Zine

Anson's Blogroll Zine is a Rust application designed to aggregate content from multiple RSS feeds, creating a personalized news page. It fetches articles from `feeds.txt` and `featured.txt`, generates HTML cards for each post, and outputs a single, styled HTML page.

## Algorithm

Long term I would like some sort of algorithm that is a small improvement over just sorting the feed by posting date. Right now the only aspects taken into account are:

- [x] Whether the feed is in the featured list
- [x] Publish date of the post
- [~] Bonus points if the feed includes an image, negative points if there isn't even a social image.
- [x] Feeds that are in the featured list are boosted

## Featured Feeds

These are feeds that are extremely high quality. Ideally they are independent blogs that don't post on a regular schedule, and don't promote anything.

## Quarantine Feeds

Feeds that I like the content of but have been acting up lately. No code touches these but they are something I will circle back to.

## RSS Feed

The site also aggregates everything that is on the homepage into an rss feed. Available at https://zine.ansonbiggs.com/feed.xml
