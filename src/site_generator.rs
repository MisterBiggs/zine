extern crate chrono;
extern crate feed_rs;
extern crate maud;
extern crate reqwest;

use chrono::DateTime;
use chrono::Utc;

use maud::{html, Markup};
use rss::{ChannelBuilder, Item, ItemBuilder};

use crate::utilities;

fn create_featured_card(post: &utilities::Post) -> Markup {
    html! {
        article class="featured" {
            header  {
                @if post.image_url.is_some() {
                    img src=(post.image_url.as_ref().unwrap()) alt="Featured image";
                }
                hgroup {
                    h2 { (post.title) }
                    a href=(format!("http://{}", post.main_url)) { (post.main_url) }
                }
            }
            body {
                p { (post.truncated_description) }
            }
            footer {
                a class="grid" href=(post.link) style="--pico-text-decoration: none;" {
                    button class="outline primary" { "Read Featured Post" }
                }
            }
        }
    }
}

fn create_post_card(post: &utilities::Post) -> Markup {
    html! {
        article {
            header {
                hgroup {
                    h2 { (post.title) }
                    a href=(format!("http://{}", post.main_url)) { (post.main_url) }
                }
            }
            body {
                @if post.image_url.is_some() {
                    img src=(post.image_url.as_ref().unwrap());
                    p;
                }
                p { (post.truncated_description) }
            }
            footer {
                a class="grid" href=(post.link) style="--pico-text-decoration: none;" {
                    button class="outline secondary" { "Read Post" }
                }
            }
        }
    }
}

fn generate_footer() -> Markup {
    let utc: DateTime<Utc> = Utc::now();
    let formatted_utc = utc.format("%Y-%m-%d %H:%M:%S").to_string();

    html! {
        footer class="container" {
            small {
                p {
                    a href="https://ansonbiggs.com" { "Anson Biggs" }
                    " - "
                    a href=("/feed.xml") target="_blank" rel="noopener noreferrer" { "RSS Feed" }
                    " - "
                    a href="https://gitlab.com/Anson-Projects/zine" { "Source Code" }
                    " - "
                    "Page generated at: " em data-tooltip="Automatic builds daily 8AM Mountain Time" { (formatted_utc) " UTC" }
                }
            }
        }
    }
}

fn generate_header() -> Markup {
    html! {
        header {
            nav {
                ul {
                    li { h1 { "The Biggs Brief" }}
                }
                ul {
                    li { button data-target="modal-about" onclick="toggleModal(event)" { "About" } }
                    li {

                    }
                    li {
                        details class="dropdown" {
                            summary role="button" class="outline secondary" { "Theme" }
                            ul {
                                li { a href="#" data-theme-switcher="auto" { "Auto" }}
                                li { a href="#" data-theme-switcher="light" { "Light" }}
                                li { a href="#" data-theme-switcher="dark" { "Dark" }}

                            }
                        }
                    }
                }
            }
        }
    }
}

fn about_modal(entries: Vec<utilities::Post>) -> Markup {
    // Get link for each post, which is a blog post then,
    // convert it to a url to the main page of the blog
    let mut links = entries
        .iter()
        .map(|post| post.main_url.as_str())
        .collect::<std::collections::HashSet<_>>()
        .into_iter()
        .collect::<Vec<_>>();

    // Alphabetical to be fair to everytone :)
    links.sort();

    html! {
        dialog id="modal-about" {
            article {
                header {
                    button aria-label="Close" rel="prev" data-target="modal-about" onclick="toggleModal(event)" {}
                    h3 { "About" }
                }
                p {
                    "I couldn't find a RSS reader that I liked so I decided to build my own."
                    "I thought it would be neat if it was public and formatted kind of like a magazine or a newspaper, so here we are."
                    "This is a feed of all the feeds that I want to keep up with. I try to keep it independant and keep out things like"
                    "enigneering blogs that are just advertisements, but its all up to my discretion."
                }
                p {
                    "This page updates daily at 8:11ish AM Mountain Time. The following blogs are"
                    " featured on the page currently:"
                }
                ul {
                    @for link in links {
                        li {a href=(format!("{}{}","http://".to_owned() , link)) {(link)}}
                    }
                }
                p {
                    "For the full list of feeds that are followed see the raw list "
                    a href="https://gitlab.com/Anson-Projects/zine/-/blob/master/feeds.txt" { "here." }
                }
            }
        }
    }
}

pub fn generate_head() -> Markup {
    html! {
        head {
            title { "Biggs Brief | Public RSS Feed" }
            meta charset="utf-8";
            meta name="viewport" content="width=device-width, initial-scale=1";
            meta name="description" content="Blogroll of RSS feeds in the format of a magazine for Anson Biggs"
            link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png";
            link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png";
            link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png";
            link rel="manifest" href="/favicon/site.webmanifest";
            link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@picocss/pico@2/css/pico.blue.min.css";
            link rel="stylesheet" href="style.css";

            // Open Graph meta tags
            meta property="og:title" content="Biggs Brief | Public RSS Feed";
            meta property="og:description" content="Blogroll of RSS feeds in the format of a magazine for Anson Biggs";
            meta property="og:url" content="https://zine.ansonbiggs.com";
            meta property="og:type" content="website";

            link rel="alternate" type="application/rss+xml" title="RSS Feed" href="/feed.xml";
        }
    }
}

fn generate_archive_table(posts: Vec<utilities::Post>) -> Markup {
    html! {
        table class="striped" {
            thead {
                tr {
                    th { "Title" }
                    th { "Date" }
                }
            }
            tbody {
                @for post in posts {
                    tr {
                        td {
                            a href=(post.link) { (post.title) }
                            br;
                            small {
                                a href=(format!("http://{}", post.main_url)) { (post.main_url) }
                            }
                        }
                        td {
                            (post.date.format("%B %d, %Y").to_string())
                        }
                    }
                }
            }
        }
    }
}

pub fn generate_index(
    mut posts: Vec<utilities::Post>,
    archive_posts: Vec<utilities::Post>,
) -> Markup {
    let featured = posts.first().unwrap().clone();
    posts.remove(0);
    log::info!(
        "Featured article: {}, img: {:?}",
        featured.link,
        featured.image_url
    );

    html! {
        (maud::DOCTYPE)
        html lang="en" {
            {(generate_head())}
            body { main class="container" {
                {(generate_header())}
                {(create_featured_card(&featured))}
                div class="grid" {
                    @for column_posts in utilities::group_by_nth(&posts, 3) {
                        div {
                            @for post in column_posts {
                                {(create_post_card(&post))}
                            }
                        }
                    }
                }
                h2 {"Random Old Posts"}
                {(generate_archive_table(archive_posts))}
                {(generate_footer())}
                {(about_modal(posts))}
                script src="modal.js" {}
                script src="minimal-theme-switcher.js" {}
            }}
        }
    }
}

pub fn generate_rss(posts: Vec<utilities::Post>) -> String {
    let items: Vec<Item> = posts
        .iter()
        .map(|post| {
            ItemBuilder::default()
                .title(post.title.clone())
                .link(post.link.clone())
                .pub_date(post.date.to_rfc2822())
                .description(post.truncated_description.clone())
                .build()
        })
        .collect();

    let channel = ChannelBuilder::default()
        .title("Anson's Blogroll")
        .link("https://zine.ansonbiggs.com/feed")
        .description("All the feeds I like, aggregated into one place.")
        .items(items)
        .build();

    channel.to_string()
}
