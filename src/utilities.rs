use crate::web_fetchers::{self, is_valid_url};
use chrono::{DateTime, Utc};
use feed_rs::model::Entry;
use rayon::prelude::*;
use scraper::{Html, Selector};

use anyhow::Result;
use html_escape::decode_html_entities;
use regex::Regex;
use std::cmp::Ordering;
use std::fs;
use url::Url;

#[derive(Clone, PartialEq, Eq)]
pub struct Post {
    pub title: String,
    pub link: String,
    pub date: DateTime<Utc>,
    pub lang: String,
    pub image_url: Option<String>,
    pub truncated_description: String,
    pub main_url: String,
    pub score: i64, // Score values still very in flux
}

impl Ord for Post {
    fn cmp(&self, other: &Self) -> Ordering {
        self.score.partial_cmp(&other.score).unwrap().reverse()
    }
}

impl PartialOrd for Post {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Post {
    fn from_entry(entry: &feed_rs::model::Entry) -> Result<Post> {
        let title = entry
            .title
            .as_ref()
            .map(|t| decode_html_entities(&t.content).into_owned())
            .unwrap_or_default();

        let link = entry
            .links
            .first()
            .ok_or_else(|| anyhow::anyhow!("No links for post {:?}", entry))?;

        let date = get_entry_date(entry);

        let lang = link.clone().href_lang.unwrap_or("en".to_string());

        if lang != "en" {
            log::warn!("Not English! {} {}", lang, link.href);
        }

        let image_url = entry
            .media
            .first()
            .and_then(|m| m.content.first())
            .and_then(|c| c.url.as_ref().map(|u| u.to_string()));

        let description = entry.content.as_ref().map_or_else(
            || {
                entry
                    .summary
                    .as_ref()
                    .map_or_else(|| "".to_string(), |summary| summary.content.clone())
            },
            |content| {
                content
                    .body
                    .as_ref()
                    .map_or_else(|| "".to_string(), |body| body.clone())
            },
        );

        let cleaned_description = strip_html_and_css_content(&description);
        let truncated_description = truncate_description(&cleaned_description, 500);

        let main_url = Url::parse(&link.href)
            .map_err(|_| anyhow::anyhow!("Failed to parse URL: {}", link.href))?
            .host_str()
            .map(String::from)
            .ok_or_else(|| anyhow::anyhow!("No host in URL: {}", link.href))?;

        let mut score = (date - (chrono::Utc::now() - chrono::Duration::days(21))).num_minutes();

        if score > 0 {
            score = score.pow(2); // I think a pow will help keep newer stuff at the top
        }

        Ok(Post {
            title,
            link: link.href.clone(),
            date,
            lang,
            image_url,
            truncated_description,
            main_url,
            score,
        })
    }
}

pub fn read_feed(path: &str) -> Vec<Post> {
    let binding = fs::read_to_string(path).unwrap();
    let feed_urls: Vec<&str> = binding.lines().collect();

    log::trace!("Fetching feeds:");

    let mut entries: Vec<Entry> = feed_urls
        .into_par_iter()
        .filter_map(|url| match web_fetchers::fetch_feed(url) {
            Ok(entries) => Some(entries),
            Err(e) => {
                println!("Failed to fetch or parse feed {}: {}", url, e);
                None
            }
        })
        .collect::<Vec<Vec<Entry>>>()
        .into_iter()
        .flatten()
        .collect();

    entries.retain(validate_entry_date);

    entries
        .par_iter()
        .map(|entry| {
            Post::from_entry(entry).map_err(|e| {
                log::warn!("Failed to process entry: {}", e);
                e
            })
        })
        .filter_map(Result::ok)
        .filter(|post| post.date < chrono::Utc::now())
        .collect::<Vec<_>>()
}

fn validate_entry_date(entry: &Entry) -> bool {
    // Check that entry has a timestamp associated with it
    if entry.published.is_some() || entry.updated.is_some() {
        // Make sure post doesn't "exist in the future"
        get_entry_date(entry) < chrono::Utc::now()
    } else {
        false
    }
}

pub fn get_entry_date(entry: &Entry) -> DateTime<Utc> {
    entry.published.unwrap_or(entry.updated.unwrap_or_default())
}

pub fn truncate_description(description: &str, max_length: usize) -> String {
    let description_trimmed = description.trim();
    if description_trimmed.len() > max_length {
        let mut char_boundary = max_length;
        for (idx, _) in description_trimmed.char_indices() {
            if idx > max_length {
                break;
            }
            char_boundary = idx;
        }
        format!("{}...", &description_trimmed[..char_boundary])
    } else {
        description_trimmed.to_string()
    }
}

pub fn strip_html_and_css_content(input: &str) -> String {
    // First, remove CSS content
    let css_regex = Regex::new(r"<style[^>]*>[\s\S]*?</style>").unwrap();
    let without_css = css_regex.replace_all(input, "");

    // Then, remove inline CSS
    let inline_css_regex = Regex::new("\\s*style\\s*=\\s*\"[^\"]*\"").unwrap();
    let without_inline_css = inline_css_regex.replace_all(&without_css, "");

    // Parse the remaining HTML and extract text
    let document = Html::parse_document(&without_inline_css);
    let selector = Selector::parse("*").unwrap();
    let mut text_content = String::new();

    for element in document.select(&selector) {
        let text = element.text().collect::<Vec<_>>().join(" ");
        text_content.push_str(&text);
        text_content.push(' ');
    }

    // Remove any remaining CSS-like content (for cases where it's not in a <style> tag)
    let final_css_regex = Regex::new(r"\.[a-zA-Z0-9_-]+\s*\{[^}]*\}").unwrap();
    let final_text = final_css_regex.replace_all(&text_content, "");

    final_text.trim().to_string()
}

pub fn group_by_nth<T: Clone>(slice: &[T], n: usize) -> Vec<Vec<T>> {
    (0..n)
        .map(|i| {
            slice
                .iter()
                .enumerate()
                .filter_map(|(index, value)| {
                    if index % n == i {
                        Some(value.clone())
                    } else {
                        None
                    }
                })
                .collect()
        })
        .collect()
}

pub fn validate(post: &mut Post) {
    if post.title.is_empty() {
        post.score = 0;
        println!("{} has no title", post.link.as_str());
        return;
    }

    if !post.lang.is_empty() && post.lang != "en" {
        post.score = 0;
        println!("{} is not english", post.link.as_str());
        return;
    }

    if post.truncated_description.is_empty() {
        post.score /= 2;
    };

    if !is_valid_url(post.link.as_str()) {
        post.score = 0;
        println!("post link {} is not valid", post.link.as_str());
        return;
    };

    if !is_valid_url(&("http://".to_owned() + post.main_url.as_str())) {
        post.score = 0;
        println!("main_url {} is not valid", post.main_url.as_str());
        return;
    };

    find_image(post);

    if let Some(image_url) = &post.image_url {
        if image_url.ends_with("favicon.ico") {
            post.image_url = None;
        }
    }
}

pub fn find_image(post: &mut Post) {
    if let Some(image_url) = &post.image_url {
        match web_fetchers::is_valid_image_url(image_url) {
            Ok(true) => {}
            _ => {
                post.image_url = None;
            }
        }
    } else {
        match web_fetchers::fetch_social_image(post.link.clone()) {
            Ok(social_image_url) => {
                if web_fetchers::is_valid_image_url(&social_image_url).unwrap_or(false) {
                    post.image_url = Some(social_image_url);
                }
            }
            Err(error) => {
                post.score = (post.score as f64 * 0.9) as i64;
                log::warn!("{}: {}", error, post.link.clone());
            }
        }
    }
}
